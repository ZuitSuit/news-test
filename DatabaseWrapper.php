<?php

//create table users (ID int NOT NULL AUTO_INCREMENT, username VARCHAR(20) NOT NULL UNIQUE, password VARCHAR(20) NOT NULL DEFAULT 'pass', ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (ID));
//create table news (ID int NOT NULL AUTO_INCREMENT, title VARCHAR(50) NOT NULL, body VARCHAR(8000) NOT NULL, userID INT NOT NULL, ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (ID));
//create table comments (ID int NOT NULL AUTO_INCREMENT, body VARCHAR(140) NOT NULL DEFAULT '-', userID INT NOT NULL, newsID INT NOT NULL, ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (ID));

class DatabaseWrapper
{
    public $connection;

    /**
     * DatabaseWrapper constructor.
     */
    public function __construct()
    {
        $this->connection = new PDO('mysql:host=localhost;dbname=sessions', 'root', 'root');
    }

    /**
     * @param string $table
     * @return array
     */
    public function all(string $table)
    {


        $query = $this->connection->query('SELECT * FROM ' . $table);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    /**
     * @param string $table
     * @param int $page
     * @param int $perPage
     * @return array
     */
    public function paginate(string $table, int $page, int $perPage)
    {
        $query = $this->connection->query('SELECT * FROM ' . $table . ' LIMIT ' . $perPage . ' OFFSET ' . ((($page - 1) * $perPage)));
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * @param string $table
     * @param int $id
     * @return array
     */
    public function getById(string $table, $id)
    {
        $query = $this->connection->query("SELECT * FROM " . $table . " WHERE ID='" . $id . "'");
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param string $table
     * @param array $requests
     * @return array
     */
    public function getByField(string $table, array $requests)
    {
        foreach ($requests as $key => $value) {
            $query = $this->connection->query(" SELECT * FROM " . $table . " WHERE " . $key . " = '" . $value . "'");
            $query->execute();
            return $query->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    /**
     * @param string $table
     * @param array $requests
     * @return bool
     */
    public function deleteByField(string $table, array $requests)
    {
        foreach ($requests as $key => $value) {
            $query = $this->connection->query(" DELETE FROM " . $table . " WHERE " . $key . " = '" . $value . "'");
            $query->execute();
            return true;
        }
    }

    /**
     * @param string $table
     * @param array $requests
     * @return bool
     */
    public function insert(string $table, array $requests)
    {


        $query = $this->connection->prepare("INSERT INTO " . $table . " (" . implode(", ",
                array_keys($requests)) . ") VALUES " . "('" . implode("', '", ($this->screen($requests))) . "')");
        $query->execute();
        print_r($query);

        return true;
    }

    /**
     * @param array $request
     * @return array
     */
    private function screen($request)
    {
        return array_map(function ($value) {
            return htmlspecialchars(trim(addslashes($value)));
        }, $request);
    }

    /**
     * @return string
     */
    public function lastId()
    {
        $result = $this->connection->lastInsertId();
        return $result;
    }

    /**
     * @param string $table
     * @return array
     */
    public function getColumns(string $table){

        $rs = $this->connection->query('SELECT * FROM ' . $table . ' LIMIT 0');
        for ($i = 0; $i < $rs->columnCount(); $i++) {
            $col = $rs->getColumnMeta($i);
            $columns[] = $col['name'];
        }

        return $columns;
    }


    /**
     * @param string $table
     * @param $id
     * @param array $requests
     */
    public function update(string $table, $id, array $requests)
    {
        $queryBlank = '';
        foreach ($requests as $key => $value) {
            $queryBlank .= $key . " = '" . $value . "', ";
        }

        $queryBlank = substr($queryBlank, 0, -2);
        $query = $this->connection->prepare("UPDATE " . $table . " SET " . $queryBlank . " WHERE ID = " . $id . "");
        $query->execute();
    }
}

