<?php

require_once "DatabaseWrapper.php";
require_once "Model.php";

class Comment extends Model
{
    protected static $table = "comments";

    /** @var string */
    protected $body;
    /** @var int */
    protected $userID;
    protected $newsID;

}