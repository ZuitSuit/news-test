<?php

abstract class Model
{
    protected $ID;
    protected $perPage = 10;
    protected static $table = "";
    protected $ts;
    protected $database;


    /**
     * Model constructor.
     * @param array $data
     */
    public function __construct(array $data) //same everywhere
    {
        global $database;
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
        $this->database = $database;
    }

    /**
     * @param $ID
     * @return Model
     */
    public static function get($ID)
    {

        $modelObject = ($GLOBALS['database']->getById(static::$table, $ID))[0];

        $class = get_called_class();

        $modelInstance = new $class($modelObject);


        return $modelInstance;

    }

    /**
     * @return array
     */
    public static function all()
    {
        $models = ($GLOBALS['database']->all(static::$table));

        $modelArray=[];

        foreach ($models as $modelObject){
            $class = get_called_class();
            $modelArray[] = new $class($modelObject);
        }

        return $modelArray;
    }

    /**
     * @return $this
     */
    public function show()
    {
        var_dump($this);

        return $this;
    }


    /**
     * @param bool $values
     * @return bool
     */
    public function save($values = false)
    {
        global $database;

        if ($values) {

            $this->update($values);

        } else {

            $fields = $database->getColumns(static::$table);


            $fieldArray = [];

            foreach ($fields as $field) {
                // exceptions for technical data, autofills and such
                if (!(($field == 'ID') || ($field == 'ts'))) {
                    $fieldArray[$field] = $this->$field;

                }
            }

            $this->database->insert(static::$table, $fieldArray);
            $this->ID = $this->database->lastId();

            return true;
        }
    }

    /**
     * @param array $values
     * @return $this
     */
    public function update(array $values)
    {
        $this->database->update(static::$table, $this->ID, $values);

        foreach ($values as $key => $value) {
            $this->$key = $value;
        }

        return $this;
    }


    /**
     * @return bool
     */
    public function delete()
    {
        $this->database->deleteByField(static::$table, ['ID' => $this->ID]);
        
        return true;
    }


    /**
     * @param int $number
     * @param bool $pages
     * @return array
     */
    public static function paginate(int $number, $pages = false)
    {
        $instance = new static([]);

        if ($pages) {

            $models = $instance->database->paginate(static::$table, $number, $pages);

        } else {

            $models = $instance->database->paginate(static::$table, $number, $instance->perPage);
        }

        $modelArray=[];

        foreach ($models as $modelObject){
            $class = get_called_class();
            $modelArray[] = new $class($modelObject);
        }

        return $modelArray;

    }

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->ID;
    }
}