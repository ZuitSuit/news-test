<?php


require_once "DatabaseWrapper.php";
require_once "Model.php";

class User extends Model
{
    protected static $table = "users";
    /** @var string */
    protected $username;
    protected $password;
    protected $timestamp = false;
}