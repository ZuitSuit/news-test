<?php session_start();

    require_once "DatabaseWrapper.php";
    require_once "signUp.php";
    require_once "User.php";
    require_once "News.php";
    require_once "Comment.php";


    $database = new DatabaseWrapper();

    $user = new User(['username' => 'Vyacheslav', 'password' => '123456']);
    $user->save();

    $article = new News(['title' => 'First Article', 'body' => '<p>Hello world</p>', 'userID' => $user->getID()]);
    $article->save();

    $comment = new Comment(['body' => 'First Comment', 'userID' => $user->getID(), 'newsID' => $article->getID()]);
    $comment->save();

    $user->show();
    $article->show();
    $comment->show();

