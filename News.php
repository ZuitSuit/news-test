<?php

require_once "DatabaseWrapper.php";
require_once "Model.php";


class News extends Model
{
    /** @var int */
    protected static $table = "news";
    /** @var string */
    protected $title;
    protected $body;
    protected $userID;
    protected $timestamp = true;

    /**
     * @return array
     */
    public function byUser()
    {
        $comments = $this->database->getByField(static::$table, ['userId' => $this->userId]);
        print_r($comments);
        return $comments;
    }



}